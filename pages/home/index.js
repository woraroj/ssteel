import React from "react";
import Head from "next/head";
import { NavBar } from "../../components";
import {
  Landing,
  Intro,
  Product,
  Project,
  News,
  Contact,
} from "../../containers/home";
import { motion } from "framer-motion";
const Home = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: 0 }}
    >
      <Head>
        <title>หน้าแรก • S.STEEL CENTER</title>
        <link rel="icon" href="/assets/logo.png" />
      </Head>
      <NavBar />
      <Landing />
      <Intro />
      <Product />
      <Project />
      {/* <News /> */}
      <Contact />
    </motion.div>
  );
};
export default Home;
