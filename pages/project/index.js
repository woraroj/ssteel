import React from "react";
import Head from "next/head";
import { NavBar, Section, Flex, Text } from "../../components";
import { Landing } from "../../containers/project";
import { Contact as ContactHome } from "../../containers/home";
import Slider from "react-slick";
import styled from "styled-components";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ProjectData } from "../../public/assets/data";
import { Container } from "reactstrap";
import { motion } from "framer-motion";
const Project = () => {
  const settings = {
    dots: true,
    infinite: true,
    centerMode: true,
    slideToShow: 1,
    slidesToScroll: 100,
    variableWidth: true,
    adaptiveHeight: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronRight}
          style={{ width: "30px", height: "30px", color: "#EC7D21" }}
          // color="red"
        />
      </div>
    );
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", left: "-35px" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronLeft}
          // color="white"
          style={{
            width: "30px",
            height: "30px",
            marginRight: "10px",
            color: "#EC7D21",
          }}
        />
      </div>
    );
  }

  const SlideWrapper = styled.div`
    margin: 4em 0;
  `;
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: 0 }}
    >
      <Head>
        <title>ผลงาน • S.STEEL CENTER</title>
        <link rel="icon" href="/assets/logo.png" />
      </Head>
      <NavBar />
      <Landing />
      <Section padding="2em 0">
        <Container>
          <Flex margin="3em 0">
            <div>
              <Text.H5 fontColor="primary">หน้าแรก</Text.H5>
            </div>
            <Text.H5 fontColor="secondary" margin="0 0.5em">
              /
            </Text.H5>
            <div>
              <Text.H5 fontColor="primary" weight={500}>
                ผลงาน
              </Text.H5>
            </div>
          </Flex>
          <Slider {...settings}>
            {ProjectData.map((data, index) => (
              <div key={index}>
                <img
                  src={data}
                  style={{
                    maxWidth: "700px",

                    margin: "0 2em",
                  }}
                />
              </div>
            ))}
          </Slider>
        </Container>
      </Section>
      <ContactHome />
    </motion.div>
  );
};
export default Project;
