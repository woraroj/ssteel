import React, { useState } from "react";
import Head from "next/head";
import { NavBar, Section, Flex, Text, Button } from "../../components";
import { Contact } from "../../containers/home";
import { Container, Collapse } from "reactstrap";
import { Landing } from "../../containers/job";
import { faChevronUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { JobData } from "../../public/assets/data";
import { motion } from "framer-motion";
const Job = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: 0 }}
    >
      <Head>
        <title>สมัครงาน • S.STEEL CENTER</title>
        <link rel="icon" href="/assets/logo.png" />
      </Head>
      <NavBar />
      <Landing />
      <Content />
      <Contact />
    </motion.div>
  );
};

const Content = () => {
  return (
    <Section>
      <Container>
        <Flex margin="3em 0">
          <div>
            <Text.H5 fontColor="primary">หน้าแรก</Text.H5>
          </div>
          <Text.H5 fontColor="secondary" margin="0 0.5em">
            /
          </Text.H5>
          <div>
            <Text.H5 fontColor="primary" weight={500}>
              สมัครงาน
            </Text.H5>
          </div>
        </Flex>
        <Flex>
          <Text.H3 className="underline" fontColor="primary">
            ตำแหน่งที่เปิดรับ
          </Text.H3>
        </Flex>
        <div style={{ margin: "2em 0" }}>
          {JobData.map((data, index) => (
            <JobWrapper data={data} index={index + 1} key={index} />
          ))}
        </div>
      </Container>
    </Section>
  );
};
const JobWrapper = ({ data, index }) => {
  const [isOpen, setOpen] = useState(false);
  console.log(data);
  return (
    <div style={{ margin: "1em 0" }}>
      <Flex
        padding="1em 2em"
        bgColor="#506A92"
        justify="space-between"
        cursor="pointer"
        onClick={() => setOpen(!isOpen)}
      >
        <Text.H4 fontColor="white">
          {index}. {data.name}
        </Text.H4>
        <FontAwesomeIcon
          icon={faChevronUp}
          width="20px"
          style={{
            color: "#EC7D21",
            transform: isOpen ? "rotate(180deg)" : "",
          }}
        />
      </Flex>
      <Collapse isOpen={isOpen}>
        <Flex
          className="shadow"
          width="100%"
          direction="column"
          padding="1em 2em"
        >
          <Text.H4 fontColor="primary" margin="0 0 0.5em 0">
            คุณสมบัติ
          </Text.H4>
          {data.requirements.map((req, reqIndex) => (
            <Text.H5 margin="1em 0 0 1em" fontColor="primary" weight={300}>
              • {req}
            </Text.H5>
          ))}

          <Text.H4 fontColor="primary" margin="2em 0 0.5em 0">
            หลักฐานการสมัคร
          </Text.H4>
          {data.document.map((req, reqIndex) => (
            <Text.H5 margin="1em 0 0 1em" fontColor="primary" weight={300}>
              • {req}
            </Text.H5>
          ))}
          <Button
            width="120px"
            margin="2em 0"
            bgColor="secondary"
            color="white"
            onClick={() =>
              window.open(
                "mailto:ssteelcenter@hotmail.com?subject=เอกสารสมัครงาน ชื่อ นามสกุล&body=หลักฐานการสมัครงาน %0d%0a • รูปถ่าย 1 ใบ %0d%0a • สำเนาบัตรประชาชน %0d%0a • สำเนาวุฒิการศึกษา %0d%0a • สำเนาใบขับขี่ %0d%0a • ชายต้องผ่านการเกณฑ์ทหาร หรือใบสค.43 %0d%0a • สำเนาทะเบียนบ้าน %0d%0a"
              )
            }
          >
            ส่งเอกสาร
          </Button>
        </Flex>
      </Collapse>
    </div>
  );
};
export default Job;
