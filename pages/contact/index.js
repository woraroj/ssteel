import React from "react";
import Head from "next/head";
import { NavBar } from "../../components";
import { Landing } from "../../containers/contact";
import { Contact as ContactHome } from "../../containers/home";
import { motion } from "framer-motion";
const Contact = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: 0 }}
    >
      <Head>
        <title>งานจัดส่ง • S.STEEL CENTER</title>
        <link rel="icon" href="/assets/logo.png" />
      </Head>
      <NavBar />
      <Landing />
      <ContactHome />
    </motion.div>
  );
};
export default Contact;
