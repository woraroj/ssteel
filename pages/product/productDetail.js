import React, { useState, useEffect } from "react";
import Head from "next/head";
import { Container } from "reactstrap";
import { NavBar, Flex, Text } from "../../components";
import { Landing, Content } from "../../containers/product";
import { Contact as ContactHome } from "../../containers/home";
import { ProductsData } from "../../public/assets/data";
import { useRouter } from "next/router";
import Slider from "react-slick";
import styled from "styled-components";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { motion } from "framer-motion";
const ProductDetail = () => {
  const router = useRouter();
  const [product, setProduct] = useState([]);

  useEffect(() => {
    const getProduct = ProductsData.filter(
      (data) => data.id === parseInt(router.query.id)
    );
    setProduct(getProduct);
  }, [router.query.id]);

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: 0 }}
    >
      <Head>
        <title>สินค้า • S.STEEL CENTER</title>
        <link rel="icon" href="/assets/logo.png" />
      </Head>
      <NavBar />
      <Landing text={product[0] && product[0].name} />
      {product.length === 0 ? (
        <Flex height="500px" justify="center" align="center">
          <Text.H1 fontColor="secondary">404 ไม่พบสินค้าที่ค้นหา</Text.H1>
        </Flex>
      ) : (
        <ProductContent product={product[0]} />
      )}

      <ContactHome />
    </motion.div>
  );
};

const ProductContent = ({ product }) => {
  const settings = {
    dots: true,
    infinite: true,
    centerMode: true,
    slideToShow: 1,
    slidesToScroll: 100,
    variableWidth: true,
    adaptiveHeight: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronRight}
          style={{ width: "30px", height: "30px", color: "#EC7D21" }}
          // color="red"
        />
      </div>
    );
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", left: "-35px" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronLeft}
          // color="white"
          style={{
            width: "30px",
            height: "30px",
            marginRight: "10px",
            color: "#EC7D21",
          }}
        />
      </div>
    );
  }

  const SlideWrapper = styled.div`
    margin: 4em 0;
  `;

  return (
    <div>
      <Container>
        <Flex margin="3em 0">
          <div>
            <Text.H5 fontColor="primary">หน้าแรก</Text.H5>
          </div>
          <Text.H5 fontColor="secondary" margin="0 0.5em">
            /
          </Text.H5>
          <div>
            <Text.H5 fontColor="primary">สินค้า</Text.H5>
          </div>
          <Text.H5 fontColor="secondary" margin="0 0.5em">
            /
          </Text.H5>
          <div>
            <Text.H5 fontColor="primary" weight={500}>
              {product.name}
            </Text.H5>
          </div>
        </Flex>
        <SlideWrapper>
          {product.picture.length > 1 ? (
            <Slider {...settings}>
              {product.picture.map((data, index) => (
                <div key={index}>
                  <img
                    src={data}
                    style={{
                      maxWidth: "450px",
                      width: "100vw",
                      margin: "0 2em",
                    }}
                  />
                </div>
              ))}
            </Slider>
          ) : (
            <Flex justify="center">
              <img src={product.picture[0]} style={{ maxWidth: "450px" }} />
            </Flex>
          )}
        </SlideWrapper>

        <Flex direction="column">
          {product.table.map((data, index) => (
            <img key={index} src={data} style={{ width: "100%" }} />
          ))}
        </Flex>
      </Container>
    </div>
  );
};
export default ProductDetail;
