import React from "react";
import Head from "next/head";
import { NavBar } from "../../components";
import { Landing, Content } from "../../containers/product";
import { Contact as ContactHome } from "../../containers/home";
import { ProductsData } from "../../public/assets/data";
import { motion } from "framer-motion";
const Product = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: 0 }}
    >
      <Head>
        <title>สินค้า • S.STEEL CENTER</title>
        <link rel="icon" href="/assets/logo.png" />
      </Head>
      <NavBar />
      <Landing text="สินค้า" />
      <Content data={ProductsData} />
      <ContactHome />
    </motion.div>
  );
};
export default Product;
