import React, { useEffect } from "react";
import { useRouter } from "next/router";
import Head from "next/head";
import styles from "../styles/Home.module.css";

const Home = () => {
  const router = useRouter();

  useEffect(() => {
    // router.push("/home");
  });

  return <div className={styles.container}></div>;
};
export default Home;
