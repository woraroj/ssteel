const path = require("path");

module.exports = {
  trailingSlash: true,
  async redirects() {
    return [
      {
        source: "/",
        destination: "/home", // Matched parameters can be used in the destination
        permanent: false,
      },
    ];
  },
  env: {
    // // api_url: "http://localhost:1337",
    // api_url: "http://172.105.174.17:1337",
  },

  webpack: (config, { isServer }) => {
    // if (!isServer) {
    //   config.node = {
    //     fs: "empty",
    //   };
    // }

    config.resolve.alias["components"] = path.join(__dirname, "components");
    config.resolve.alias["public"] = path.join(__dirname, "public");

    return config;
  },
  resolve: {
    extensions: [".js", ".jsx"],
  },
};
