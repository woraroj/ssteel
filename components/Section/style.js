import styled from "styled-components";

export const SectionStyled = styled.div`
  height: ${(props) => props.height};
  background-image: ${(props) => props.bg};
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  min-height: ${(props) => props.minHeight};
  padding: ${(props) => props.padding};
  margin: ${props=> props.margin};
  &.middle{
    display:flex;
    justify-content:center;
    align-items:center;
  }
  &.filter {
    background-image: linear-gradient(rgb(0, 0, 0, 0.15), rgb(0, 0, 0, 0.15)),
      url(${(props) => props.bg});
  }
  &.filter2 {
    background-image: linear-gradient(rgb(0, 0, 0, 0.5), rgb(0, 0, 0, 0.5)),
      url(${(props) => props.bg});
  }
`;
