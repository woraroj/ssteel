import React from "react";
import { SectionStyled } from "./style";

export const Section = (props) => {
  const { children, height, bg, filter, className, minHeight, padding,margin } = props;
  return (
    <SectionStyled
      height={height}
      minHeight={minHeight}
      bg={bg}
      filter={filter}
      className={className}
      padding={padding}
      margin={margin}
    >
      {children}
    </SectionStyled>
  );
};
