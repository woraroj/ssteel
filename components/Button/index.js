import React from "react";
import { ButtonStyle } from "./style";

export const Button = (props) => {
  const { children, className, bgColor, color, width, margin, onClick } = props;
  return (
    <ButtonStyle
      bgColor={bgColor}
      className={className}
      color={color}
      width={width}
      margin={margin}
      onClick={onClick}
    >
      {children}
    </ButtonStyle>
  );
};
