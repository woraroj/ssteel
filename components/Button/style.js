import styled from "styled-components";

export const ButtonStyle = styled.button`
  color: ${(props) => props.color};
  background-color: ${(props) => props.theme.color[props.bgColor]};
  border: 1px solid ${(props) => props.theme.color[props.bgColor]};
  width: ${(props) => props.width};
  border-radius: 5px;
  margin: ${(props) => props.margin};
  font-weight: 200;
  transition-duration: 0.25s;
  &.show {
    opacity: 1;
    visibility: visible;
    transition: visibility 0.3s linear, opacity 0.3 linear;
  }
  &.hide {
    opacity: 0;
    visibility: hidden;
    transition: visibility 0.3s linear, opacity 0.3 linear;
  }
  &.outline {
    color: ${(props) => props.theme.color[props.bgColor]};

    background-color: rgb(0, 0, 0, 0);
    border: 1px solid ${(props) => props.theme.color[props.bgColor]};
  }
  :hover {
    color: ${(props) => props.theme.color[props.bgColor]};
    background-color: ${(props) => props.theme.color[props.color]};
    border: 1px solid ${(props) => props.theme.color[props.bgColor]};
  }
`;
