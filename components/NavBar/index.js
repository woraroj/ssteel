import React, { useState } from "react";
import {
  HamburgerWrapper,
  SpanStyle,
  SlideDrawerStyled,
  NavWrapper,
  SubNavWrapper,
} from "./style";
import { Flex, Text } from "../index";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebookF, faLine } from "@fortawesome/free-brands-svg-icons";
import {
  faHome,
  faShoppingCart,
  faPeopleCarry,
  faBuilding,
  faBullhorn,
  faAddressBook,
  faSuitcase,
} from "@fortawesome/free-solid-svg-icons";
import { useRouter } from "next/router";
import Link from "next/link";

export const NavBar = (props) => {
  const [isCollapsed, setCollapsed] = useState(true);
  const router = useRouter();

  const onToggle = () => {
    setCollapsed(!isCollapsed);
  };
  return (
    <NavWrapper>
      <Hamburger isCollapsed={isCollapsed} setCollapsed={setCollapsed} />
      <SlideDrawerStyled collapsed={isCollapsed}>
        <Flex justify="center" direction="column" align="flex-start">
          <Link href="/home">
            <a>
              <SubNavWrapper
                className={router.route.includes("home") && "active"}
                onClick={() => onToggle()}
              >
                <FontAwesomeIcon
                  icon={faHome}
                  style={{
                    color: router.route.includes("home") && "#506A92",
                    width: "30px",
                  }}
                />
                <Text.H6
                  fontColor={
                    router.route.includes("home") ? "primary" : "white"
                  }
                  weight={300}
                >
                  หน้าแรก
                </Text.H6>
              </SubNavWrapper>
            </a>
          </Link>
          <Link href="/product">
            <a>
              <SubNavWrapper
                className={router.route.includes("product") && "active"}
                onClick={() => onToggle()}
              >
                <FontAwesomeIcon
                  icon={faShoppingCart}
                  style={{
                    color: router.route.includes("product") && "#506A92",
                    width: "30px",
                  }}
                />
                <Text.H6
                  fontColor={
                    router.route.includes("product") ? "primary" : "white"
                  }
                  weight={300}
                >
                  สินค้า
                </Text.H6>
              </SubNavWrapper>
            </a>
          </Link>
          <Link href="/delivery">
            <a>
              <SubNavWrapper
                className={router.route.includes("delivery") && "active"}
                onClick={() => onToggle()}
              >
                <FontAwesomeIcon
                  icon={faPeopleCarry}
                  style={{
                    color: router.route.includes("delivery") && "#506A92",
                    width: "30px",
                  }}
                />
                <Text.H6
                  fontColor={
                    router.route.includes("delivery") ? "primary" : "white"
                  }
                  weight={300}
                >
                  งานจัดส่ง
                </Text.H6>
              </SubNavWrapper>
            </a>
          </Link>
          <Link href="/project">
            <a>
              <SubNavWrapper
                className={router.route.includes("project") && "active"}
                onClick={() => onToggle()}
              >
                <FontAwesomeIcon
                  icon={faBuilding}
                  style={{
                    color: router.route.includes("project") && "#506A92",
                    width: "30px",
                  }}
                />
                <Text.H6
                  fontColor={
                    router.route.includes("project") ? "primary" : "white"
                  }
                  weight={300}
                >
                  ผลงาน
                </Text.H6>
              </SubNavWrapper>
            </a>
          </Link>
          {/* <Link href="/news">
            <a>
              <SubNavWrapper
                className={router.route.includes("news") && "active"}
                onClick={() => onToggle()}
              >
                <FontAwesomeIcon
                  icon={faBullhorn}
                  style={{
                    color: router.route.includes("news") && "#506A92",
                    width: "30px",
                  }}
                />
                <Text.H6
                  fontColor={
                    router.route.includes("news") ? "primary" : "white"
                  }
                  textAlign="center"
                  weight={300}
                >
                  ข่าวสารและสาระน่ารู้
                </Text.H6>
              </SubNavWrapper>
            </a>
          </Link> */}
          <Link href="/contact">
            <a>
              <SubNavWrapper
                className={router.route.includes("contact") && "active"}
                onClick={() => onToggle()}
              >
                <FontAwesomeIcon
                  icon={faAddressBook}
                  style={{
                    color: router.route.includes("contact") && "#506A92",
                    width: "30px",
                  }}
                />
                <Text.H6
                  fontColor={
                    router.route.includes("contact") ? "primary" : "white"
                  }
                  weight={300}
                >
                  ติดต่อเรา
                </Text.H6>
              </SubNavWrapper>
            </a>
          </Link>
          <Link href="/job">
            <a>
              <SubNavWrapper
                className={router.route.includes("job") && "active"}
                onClick={() => onToggle()}
              >
                <FontAwesomeIcon
                  icon={faSuitcase}
                  style={{
                    color: router.route.includes("job") && "#506A92",
                    width: "30px",
                  }}
                />
                <Text.H6
                  fontColor={router.route.includes("job") ? "primary" : "white"}
                  weight={300}
                >
                  สมัครงาน
                </Text.H6>
              </SubNavWrapper>
            </a>
          </Link>
        </Flex>
      </SlideDrawerStyled>
    </NavWrapper>
  );
};

const Hamburger = (props) => {
  const { setCollapsed, isCollapsed } = props;

  return (
    <HamburgerWrapper onClick={() => setCollapsed(!isCollapsed)}>
      <SpanStyle
        className="one"
        // bgColor={bgColor}
        // hoverColor={collapsedColor}
        collapsed={isCollapsed}
      ></SpanStyle>
      <SpanStyle
        // bgColor={bgColor}
        // hoverColor={collapsedColor}
        collapsed={isCollapsed}
        className="two"
      ></SpanStyle>
      <SpanStyle
        className="three"
        // bgColor={bgColor}
        // hoverColor={collapsedColor}
        collapsed={isCollapsed}
      ></SpanStyle>
    </HamburgerWrapper>
  );
};
