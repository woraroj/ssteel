import styled from "styled-components";

export const NavWrapper = styled.div`
  position: sticky;
  top: 0;
  display: flex;
  height: 70px;
  z-index: 99;
`;
export const HamburgerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-left: 40px;
  height: 70px;
  width: 33px;
  z-index: 99 !important;
  cursor: pointer;
`;

export const SpanStyle = styled.span`
  display: block;

  height: 4px;
  cursor: pointer;
  margin-bottom: 5px;
  position: relative;
  z-index: 99;
  border-radius: 3px;
  transform-origin: 0px 0px;
  transition: transform 0.5s cubic-bezier(0.77, 0.2, 0.05, 1);
  background: 0.5s cubic-bezier(0.77, 0.2, 0.05, 1);
  opacity: 0.55s ease;
  &.one {
    width: 33px;
    transform: ${(props) =>
      props.collapsed ? "none" : "rotate(45deg) translate(-2px,0px)"};
    background: ${(props) =>
      props.collapsed
        ? `${props.theme.color.white}`
        : `${props.theme.color.white}`};
    transform-origin: ${(props) => (props.collapsed ? "0% 0%" : "none")};
  }
  &.two {
    width: 20px;
    transform-origin: ${(props) => (props.collapsed ? "0% 100%" : "none")};
    opacity: ${(props) => (props.collapsed ? "1" : "0")};
    background: ${(props) =>
      props.collapsed ? `${props.theme.color.white}` : `${props.hoverColor}`};
    transform: ${(props) =>
      props.collapsed ? "none" : "rotate(0deg) scale(0.2, 0.2)"};
  }
  &.three {
    width: ${(props) => (props.collapsed ? `12px` : `33px`)};
    transform: ${(props) =>
      props.collapsed ? "none" : "rotate(-45deg) translate(-5px, 0)"};
    background: ${(props) =>
      props.collapsed
        ? `${props.theme.color.white}`
        : `${props.theme.color.white}`};
  }
`;

export const SlideDrawerStyled = styled.div`
  height: 100%;
  background: ${(props) => props.theme.color.primary};
  position: fixed;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  padding-top: 70px;
  top: 0;
  overflow: scroll;
  width: 100px;
  z-index: 90;
  box-shadow: 1px 0px 7px rgba(0, 0, 0, 0.5);
  transform: ${(props) =>
    props.collapsed ? "translateX(-100%)" : "translateX(0)"};
  transition: transform 0.5s ease-out;
`;

export const SubNavWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100px;
  height: 80px;
  color: white;
  cursor: pointer;
  &.active {
    background: white;
  }
  :hover {
    color: #ec7d21;
  }
`;
