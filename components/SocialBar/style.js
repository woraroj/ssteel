import styled from "styled-components";

export const Text = styled.p`
  color: rgb(255, 255, 255, 0.8);
  font-weight: 300;
  font-size: 18px;
  letter-spacing: 1px;
  text-shadow: 2px 2px 4px #000000;
`;

export const LogoWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  font-size: 25px;
  /* background-color: rgb(255, 255, 255, 0.5); */
  color: white;
  width: 35px;
  height: 35px;
  margin: 0 10px;
  transition-duration: 0.25s;
  cursor: pointer;
  :hover {
    color: #ec7d21;
  }
`;
