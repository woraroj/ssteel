import React from "react";
import { Flex } from "../index";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebookSquare, faLine } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { Text, LogoWrapper } from "./style";
export const SocialBar = ({ center }) => {
  const handleClick = (url) => {
    window.open(url, "_blank");
  };
  return (
    <Flex direction="column"  align="center">
      <Text>ติดตามเราได้ที่</Text>
      <Flex>
        <LogoWrapper
          onClick={() =>
            handleClick("https://www.facebook.com/TransAdSolutions")
          }
        >
          <FontAwesomeIcon icon={faFacebookSquare} style={{ width: "30px" }} />
        </LogoWrapper>
        <LogoWrapper
          onClick={() => handleClick("https://twitter.com/TransAdSolution")}
        >
          <FontAwesomeIcon icon={faLine} style={{ width: "30px" }} />
        </LogoWrapper>
        <LogoWrapper
          onClick={() => handleClick("https://twitter.com/TransAdSolution")}
        >
          <FontAwesomeIcon icon={faEnvelope} style={{ width: "40px" }} />
        </LogoWrapper>
      </Flex>
    </Flex>
  );
};
