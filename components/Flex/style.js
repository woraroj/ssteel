import styled from "styled-components";

export const FlexStyle = styled.div`
  display: flex;
  flex-direction: ${(props) => props.direction};
  justify-content: ${(props) => props.justify};
  align-items: ${(props) => props.align};
  margin: ${(props) => props.margin};
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  padding: ${(props) => props.padding};
  background-image: url(${(props) => props.bg});
  background-color: ${(props) => props.bgColor};
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  transition-duration: 2s;
  flex-wrap: ${(props) => props.wrap};
  cursor: ${(props) => props.cursor};
  &.filter {
    background-image: linear-gradient(rgb(0, 0, 0, 0.5), rgb(0, 0, 0, 0.5)),
      url(${(props) => props.bg});
  }
  &.filter2 {
    background-image: linear-gradient(rgb(0, 0, 0, 0.2), rgb(0, 0, 0, 0.2)),
      url(${(props) => props.bg});
  }
  &.shadow {
    -webkit-box-shadow: 10px 10px 5px -8px rgba(153, 153, 153, 1);
    -moz-box-shadow: 10px 10px 5px -8px rgba(153, 153, 153, 1);
    box-shadow: 10px 10px 5px -8px rgba(153, 153, 153, 1);
  }
  @media only screen and (max-width: 768px) {
    &.responsive-hide {
      display: none;
    }
    &.responsive-full-width {
      width: 100%;
    }
    &.responsive-full-height {
      height: 100%;
    }
    &.responsive-half-width {
      width: 50%;
    }
    &.responsive-justify-center {
      justify-content: center;
    }
    &.responsive-align-center {
      align-items: center;
    }
    &.responsive-column {
      flex-direction: column;
    }
    &.responsive-order-0 {
      order: 0;
    }
    &.responsive-order-1 {
      order: 1;
    }
    &.responsive-height250px {
      height: 250px;
    }
    &.responsive-margin0 {
      margin: 0;
    }
  }
`;
