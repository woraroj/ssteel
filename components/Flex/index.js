import React from "react";
import { FlexStyle } from "./style";

export const Flex = (props) => {
  const {
    children,
    id,
    justify,
    align,
    direction,
    margin,
    width,
    padding,
    height,
    className,
    bg,
    bgColor,
    onClick,
    onMouseOver,
    onMouseLeave,
    wrap,
    cursor,
  } = props;
  return (
    <FlexStyle
      id={id}
      justify={justify}
      align={align}
      direction={direction}
      margin={margin}
      width={width}
      height={height}
      padding={padding}
      className={className}
      bg={bg}
      bgColor={bgColor}
      onClick={onClick}
      onMouseOver={onMouseOver}
      onMouseLeave={onMouseLeave}
      wrap={wrap}
      cursor={cursor}
    >
      {children}
    </FlexStyle>
  );
};
