export default {
  primary: "#506A92",
  secondary: "#EC7D21",
  white: "rgb(245,245,245)",
  lightBlue: "rgb(23,153,245)",
  grey: "#282c34",
  lightGrey: "rgb(230,230,230)",
  black: "rgb(0,0,0)",
  example: "rgb(12,84,140,0.4)",
};
