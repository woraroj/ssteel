import React, { useState } from "react";
import { Section, Flex, Text, Button, SocialBar } from "../../components";
import { Container } from "reactstrap";
import Link from "next/link";
export const Content = ({ data }) => {
  const [onHover, setHover] = useState("");
  return (
    <Section padding="2em 0">
      <Container>
        <Flex margin="3em 0">
          <div>
            <Text.H5 fontColor="primary">หน้าแรก</Text.H5>
          </div>
          <Text.H5 fontColor="secondary" margin="0 0.5em">
            /
          </Text.H5>
          <div>
            <Text.H5 fontColor="primary" weight={500}>
              สินค้า
            </Text.H5>
          </div>
        </Flex>
        <Flex wrap="wrap">
          {data.map((eachData, index) => (
            <ProductWrapper
              key={eachData.id}
              product={eachData}
              onHover={onHover}
              setHover={setHover}
            />
          ))}
        </Flex>
      </Container>
    </Section>
  );
};

const ProductWrapper = ({ product, onHover, setHover }) => {
  return (
    <Flex
      onMouseOver={() => setHover(product.id)}
      onMouseLeave={() => setHover("")}
      className="filter"
      direction="column"
      height="300px"
      width="50%"
      justify="center"
      align="center"
      bg={product.picture[0]}
    >
      <Text.H4 weight={onHover === product.id ? 500 : 200} fontColor="white">
        {product.name}
      </Text.H4>
      <Link href={`/product/productDetail?id=${product.id}`}>
        <a>
          <Button
            className={onHover === product.id ? "show" : "hide"}
            margin="0.5em  0"
            width="150px"
            bgColor="secondary"
            color="white"
          >
            ดูสินค้า
          </Button>
        </a>
      </Link>
    </Flex>
  );
};
