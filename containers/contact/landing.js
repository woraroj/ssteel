import React from "react";
import { Section, Flex, Text, Button, SocialBar } from "../../components";
import { Container } from "reactstrap";

export const Landing = () => {
  return (
    <Section
      className="filter2 middle"
      height="75vh"
      bg="/assets/contact/bg.png"
      margin="-70px 0 0 0"
    >
      <Container>
        <Flex
          direction="column"
          align="center"
          margin="-70px 0 0 0"
          padding="10% 0 0 0"
        >
          <Text.H2 weight={600} fontColor="white">
            ติดต่อเรา
          </Text.H2>
          <Text.H4
            className="text-shadow"
            margin="0.5em 0"
            weight={300}
            fontColor="white"
          >
            บริษัท เอส.สตีล เซ็นเตอร์ จำกัด
          </Text.H4>

          <SocialBar center={true} />
        </Flex>
      </Container>
    </Section>
  );
};
