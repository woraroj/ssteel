export * from "./home";
export * from "./product";
export * from "./delivery";
export * from "./project";
export * from "./news";
export * from "./contact";
