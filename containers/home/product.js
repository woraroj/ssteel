import React, { useState } from "react";
import { Section, Text, Flex, Button } from "../../components";
import { Container } from "reactstrap";
import Link from "next/link";
export const Product = () => {
  const [onHover, setHover] = useState("");
  return (
    <Section minHeight="500px" padding="100px 0">
      <Container>
        <Flex>
          <Text.H3 className="underline" fontColor="primary">
            สินค้าของเรา
          </Text.H3>
        </Flex>
        <Flex margin="3em 0 0 0" className="responsive-column">
          <Flex
            direction="column"
            onMouseOver={() => setHover("1")}
            onMouseLeave={() => setHover("")}
            className={
              onHover !== "1"
                ? "filter responsive-full-width "
                : "filter2 responsive-full-width "
            }
            justify="center"
            align="center"
            bg="/assets/home/product1.png"
            width="50%"
            height="300px"
          >
            <Text.H4 weight={300} fontColor="white">
              เหล็กเส้นกลม
            </Text.H4>
            <Link href="/product/productDetail?id=1">
              <a>
                <Button
                  className={onHover === "1" ? "show" : "hide"}
                  margin="0.5em  0"
                  width="150px"
                  bgColor="primary"
                  color="white"
                >
                  ดูสินค้า
                </Button>
              </a>
            </Link>
          </Flex>
          <Flex
            direction="column"
            onMouseOver={() => setHover("2")}
            onMouseLeave={() => setHover("")}
            className={
              onHover !== "2"
                ? "filter responsive-full-width "
                : "filter2 responsive-full-width "
            }
            justify="center"
            align="center"
            bg="/assets/home/product2.png"
            width="50%"
            height="300px"
          >
            <Text.H4 weight={300} fontColor="white">
              เหล็กข้ออ้อย
            </Text.H4>
            <Link href="/product/productDetail?id=2">
              <a>
                <Button
                  className={onHover === "2" ? "show" : "hide"}
                  margin="0.5em  0"
                  width="150px"
                  bgColor="primary"
                  color="white"
                >
                  ดูสินค้า
                </Button>
              </a>
            </Link>
          </Flex>
        </Flex>
        <Flex className="responsive-column">
          <Flex
            direction="column"
            onMouseOver={() => setHover("3")}
            onMouseLeave={() => setHover("")}
            className={
              onHover !== "3"
                ? "filter responsive-full-width "
                : "filter2 responsive-full-width "
            }
            justify="center"
            align="center"
            bg="/assets/home/product3.png"
            width="50%"
            height="300px"
          >
            <Text.H4 weight={300} fontColor="white">
              เหล็กท่อกลมดำ
            </Text.H4>
            <Link href="/product/productDetail?id=3">
              <a>
                <Button
                  className={onHover === "3" ? "show" : "hide"}
                  margin="0.5em  0"
                  width="150px"
                  bgColor="primary"
                  color="white"
                >
                  ดูสินค้า
                </Button>
              </a>
            </Link>
          </Flex>
          <Flex
            direction="column"
            onMouseOver={() => setHover("4")}
            onMouseLeave={() => setHover("")}
            className={
              onHover !== "4"
                ? "filter responsive-full-width "
                : "filter2 responsive-full-width "
            }
            justify="center"
            align="center"
            bg="/assets/home/product4.png"
            width="50%"
            height="300px"
          >
            <Text.H4 weight={300} fontColor="white">
              ดูสินค้าอื่นๆ
            </Text.H4>
            <Link href="/product">
              <a>
                <Button
                  className={onHover === "4" ? "show" : "hide"}
                  margin="0.5em  0"
                  width="150px"
                  bgColor="primary"
                  color="white"
                >
                  ดูสินค้า
                </Button>
              </a>
            </Link>
          </Flex>
        </Flex>
      </Container>
    </Section>
  );
};
