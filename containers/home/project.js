import React, { useState } from "react";
import { Section, Text, Flex, Button } from "../../components";
import { Container } from "reactstrap";
import Link from "next/link";
export const Project = () => {
  const [onHover, setHover] = useState("");
  return (
    <Section minHeight="500px" padding="100px 0">
      <Container>
        <Flex>
          <Text.H3 className="underline" fontColor="primary">
            ผลงานของเรา
          </Text.H3>
        </Flex>
        <Flex margin="3em 0 0 0" className="responsive-column">
          <Flex
            direction="column"
            onMouseOver={() => setHover("1")}
            onMouseLeave={() => setHover("")}
            className={
              onHover !== "1"
                ? "filter responsive-full-width "
                : "filter2 responsive-full-width "
            }
            justify="center"
            align="center"
            bg="/assets/home/project1.png"
            width="50%"
            height="300px"
          >
            <Text.H4 weight={300} fontColor="white">
              ผลงาน 1
            </Text.H4>
            <Link href="/project">
              <a>
                <Button
                  className={onHover === "1" ? "show" : "hide"}
                  margin="0.5em  0"
                  width="150px"
                  bgColor="primary"
                  color="white"
                >
                  ดูผลงาน
                </Button>
              </a>
            </Link>
          </Flex>
          <Flex
            direction="column"
            onMouseOver={() => setHover("2")}
            onMouseLeave={() => setHover("")}
            className={
              onHover !== "2"
                ? "filter responsive-full-width "
                : "filter2 responsive-full-width "
            }
            justify="center"
            align="center"
            bg="/assets/home/project2.png"
            width="50%"
            height="300px"
          >
            <Text.H4 weight={300} fontColor="white">
              ผลงาน 2
            </Text.H4>
            <Link href="/project">
              <a>
                <Button
                  className={onHover === "2" ? "show" : "hide"}
                  margin="0.5em  0"
                  width="150px"
                  bgColor="primary"
                  color="white"
                >
                  ดูผลงาน
                </Button>
              </a>
            </Link>
          </Flex>
        </Flex>
        <Flex>
          <Flex
            direction="column"
            onMouseOver={() => setHover("3")}
            onMouseLeave={() => setHover("")}
            className={onHover !== "3" ? "filter" : "filter2"}
            justify="center"
            align="center"
            bg="/assets/home/project3.png"
            width="100%"
            height="300px"
          >
            <Text.H4 weight={300} fontColor="white">
              ผลงานอื่นๆ
            </Text.H4>
            <Link href="/project">
              <a>
                <Button
                  className={onHover === "3" ? "show" : "hide"}
                  margin="0.5em  0"
                  width="150px"
                  bgColor="primary"
                  color="white"
                >
                  ดูผลงาน
                </Button>
              </a>
            </Link>
          </Flex>
        </Flex>
      </Container>
    </Section>
  );
};
