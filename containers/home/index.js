export * from "./landing";
export * from "./intro";
export * from "./product";
export * from "./project";
export * from "./news";
export * from "./contact";
