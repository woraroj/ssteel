import React from "react";
import { Section, Flex, Text } from "components";
import { Container, Badge } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLine } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
export const Contact = () => {
  return (
    <Section padding="2em 0 0 0">
      <Container>
        <Flex>
          <Text.H3 className="underline" fontColor="primary">
            ติดต่อเรา
          </Text.H3>
        </Flex>
        <Flex className="responsive-column" margin="50px 0" width="100%">
          <Flex className="responsive-full-width" width="50%">
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d247842.33715195127!2d100.15325537299535!3d13.926629324608522!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e2896d38c3ae89%3A0x5e820c9b53a0db6a!2sS.%20Steel!5e0!3m2!1sen!2sth!4v1640908824392!5m2!1sen!2sth"
              style={{ border: "0", width: "100%" }}
              allowFullScreen=""
              loading="lazy"
            ></iframe>
          </Flex>
          <Flex
            className="responsive-full-width"
            direction="column"
            width="50%"
            padding="1em"
          >
            <Text.H3 fontColor="primary">
              บริษัท เอส.สตีล เซ็นเตอร์ จำกัด
            </Text.H3>
            <Text.H5 margin="2em 0 0 0" fontColor="primary">
              28/6 ม.1 ต.หน้าไม้ อ.ลาดหลุมแก้ว จ.ปทุมธานี 12140
            </Text.H5>
            <Text.H5 margin="2em 0 0 0" fontColor="primary">
              โทร : 0-2977-7702-4
            </Text.H5>
            <Text.H5 margin="2em 0 0 0" fontColor="primary">
              แฟ็กซ์ : 0-2977-7705
            </Text.H5>
            <Text.H5 margin="2em 0 0 0" fontColor="primary">
              เวลาทำการ : วันจันทร์ - วันเสาร์ 08:00-17:00 น.
            </Text.H5>
          </Flex>
        </Flex>
        <Flex>
          <Text.H3 className="underline" fontColor="primary">
            ติดต่อฝ่ายขาย
          </Text.H3>
        </Flex>
        <Flex className="responsive-column" margin="50px 0" width="100%">
          <Flex
            className="responsive-full-width"
            direction="column"
            width="50%"
          >
            <Text.H5 margin="1em 0 0 0" fontColor="primary">
              คุณนนลณีย์ อุดมา (คุณนุ้ย)
            </Text.H5>
            <Text.H6 weight={200} fontColor="primary">
              Tel : 0867710033 LINE ID : ssteel
            </Text.H6>
            <Text.H5 margin="2em 0 0 0" fontColor="primary">
              คุณรินทร์ลภัส อุดมา (คุณแนน)
            </Text.H5>
            <Text.H6 weight={200} fontColor="primary">
              Tel : 0858116696 LINE ID : s.steel
            </Text.H6>
            <Text.H5 margin="2em 0 0 0" fontColor="primary">
              คุณอรพรรณ สุขสวัสดิ์ (คุณแอ๋ม)
            </Text.H5>
            <Text.H6 weight={200} fontColor="primary">
              Tel : 0844593395 LINE ID : ssteel.
            </Text.H6>
            <Text.H5 margin="2em 0 0 0" fontColor="primary">
              คุณจิรัฏฐ์ ชิตะธรรมพัฒน์ (คุณเบน)
            </Text.H5>
            <Text.H6 weight={200} fontColor="primary">
              Tel : 0804258465 LINE ID : 0804258465
            </Text.H6>
            <Text.H5 margin="2em 0 0 0" fontColor="primary">
              คุณนฤชิต ปานปิ่นศิลป์ (คุณติ๊ก)
            </Text.H5>
            <Text.H6 weight={200} fontColor="primary">
              Tel : 0805698248 LINE ID : 0805698248
            </Text.H6>
          </Flex>
          <Flex
            className="responsive-full-width"
            align="flex-start"
            direction="column"
            width="50%"
          >
            <Flex align="center">
              <FontAwesomeIcon
                icon={faLine}
                style={{ width: "40px", color: "#506A92" }}
              />
              <Text.H4 fontColor="primary" margin="0 0 0 1em">
                : @ssteel
              </Text.H4>
            </Flex>
            <Flex align="center" margin="2em 0 0 0">
              <FontAwesomeIcon
                icon={faEnvelope}
                style={{ width: "40px", color: "#506A92" }}
              />
              <Text.H4 fontColor="primary" margin="0 0 0 1em">
                : ssteelcenter@hotmail.com
              </Text.H4>
            </Flex>
          </Flex>
        </Flex>
      </Container>
      <Flex
        bgColor="#506A92"
        justify="center"
        align="center"
        direction="column"
        padding="2em 0"
      >
        <Text.H5 fontColor="white">
          Copyright © 2021 บริษัท เอส.สตีล เซ็นเตอร์ จำกัด All rights reserved.
        </Text.H5>
        <Text.H6 fontColor="white" weight={200}>
          Design and Develop by
          <a target="_blank" href="https://worarojs.github.io/myPort/">
            <Badge
              className="p-2 m-1"
              style={{ cursor: "woraroj " }}
              color="secondary"
            >
              Chin Su
            </Badge>
          </a>
          associated with{" "}
          <a target="_blank" href="http://aitechnovation.com/">
            AI Technovation Co.,Ltd.
          </a>
        </Text.H6>
      </Flex>
    </Section>
  );
};
