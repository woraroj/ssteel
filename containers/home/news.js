import React, { useState } from "react";
import { Section, Text, Flex, Button } from "../../components";
import { Container } from "reactstrap";
export const News = () => {
  const [onHover, setHover] = useState("");
  return (
    <Section minHeight="500px" padding="100px 0">
      <Container>
        <Flex>
          <Text.H3 className="underline" fontColor="primary">
            ข่าว
          </Text.H3>
          <Button
            className="outline"
            margin="0 4em"
            width="100px"
            bgColor="primary"
            color="white"
          >
            ดูทั้งหมด
          </Button>
        </Flex>
        <Flex
          className="responsive-column "
          justify="space-around"
          margin="2em 0"
          wrap="wrap"
        >
          <Flex
            onMouseOver={() => setHover("1")}
            onMouseLeave={() => setHover("")}
            className="filter"
            bg="/assets/home/news.png"
            width="300px"
            height="450px"
            justify="flex-end"
            align="flex-start"
            padding="15px"
            direction="column"
            margin="2em"
          >
            <Text.H4 fontColor="white">ข่าวสารน่ารู้</Text.H4>
            {onHover === "1" && (
              <Button
                margin="1em 0"
                width="100px"
                bgColor="primary"
                color="white"
              >
                กดเพื่ออ่าน
              </Button>
            )}
          </Flex>
          <Flex
            onMouseOver={() => setHover("2")}
            onMouseLeave={() => setHover("")}
            className="filter"
            bg="/assets/home/news.png"
            width="300px"
            height="450px"
            justify="flex-end"
            align="flex-start"
            padding="15px"
            direction="column"
            margin="2em"
          >
            <Text.H4 fontColor="white">ข่าวสารน่ารู้</Text.H4>
            {onHover === "2" && (
              <Button
                margin="1em 0"
                width="100px"
                bgColor="primary"
                color="white"
              >
                กดเพื่ออ่าน
              </Button>
            )}
          </Flex>
          <Flex
            onMouseOver={() => setHover("3")}
            onMouseLeave={() => setHover("")}
            className="filter"
            bg="/assets/home/news.png"
            width="300px"
            height="450px"
            justify="flex-end"
            align="flex-start"
            padding="15px"
            direction="column"
            margin="2em"
          >
            <Text.H4 fontColor="white">ข่าวสารน่ารู้</Text.H4>
            {onHover === "3" && (
              <Button
                margin="1em 0"
                width="100px"
                bgColor="primary"
                color="white"
              >
                กดเพื่ออ่าน
              </Button>
            )}
          </Flex>
        </Flex>
        <Flex
          margin="5em 0 0 0"
          bg="/assets/home/job.png"
          className="filter"
          width="100%"
          height="300px"
          direction="column"
          justify="center"
          align="center"
        >
          <Text.H3 fontColor="white">ประกาศรับสมัครงาน</Text.H3>
          <Text.H5 fontColor="white">ร่วมงานกับเรา</Text.H5>
          <Button margin="1em 0" width="200px" bgColor="primary" color="white">
            ดูตำแหน่งที่เปิดรับที่นี่
          </Button>
        </Flex>
      </Container>
    </Section>
  );
};
