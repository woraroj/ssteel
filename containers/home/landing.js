import React from "react";
import { Section, Flex, Text, Button, SocialBar } from "../../components";
import { Container } from "reactstrap";
import { BG } from "../../public/assets/home/bg.png";
export const Landing = () => {
  return (
    <Section className="filter" height="100vh" bg="/assets/home/bg.png">
      <Container>
        <Flex
          direction="column"
          align="flex-start"
          margin="-70px 0 0 0"
          padding="15% 0 0 0"
        >
          <Text.H2 weight={600} fontColor="white">
            S.STEEL CENTER
          </Text.H2>
          <Text.H4
            className="text-shadow"
            margin="0.5em 0"
            weight={300}
            fontColor="white"
          >
            ซื้อสัตย์ บริการรวดเร็ว เหล็กครบ จบในที่เดียว
          </Text.H4>
          <a href="#aboutus">
            <Button
              margin="0.5em 0 4em 0"
              width="150px"
              bgColor="secondary"
              color="white"
            >
              เกี่ยวกับเรา
            </Button>
          </a>

          <SocialBar />
          <div
            className="responsive-hide"
            style={{ position: "absolute", right: "5%", bottom: "0" }}
          >
            <img src="/assets/home/1.png" />
          </div>
        </Flex>
      </Container>
    </Section>
  );
};
