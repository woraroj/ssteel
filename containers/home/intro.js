import React from "react";
import { Section, Flex, Text, Button } from "../../components";
import { Container } from "reactstrap";
import Link from "next/link";
export const Intro = () => {
  return (
    <Section minHeight="500px" padding="100px 0">
      <Container>
        <Flex id="aboutus" width="100%" justify="center">
          <Flex
            className="shadow responsive-column responsive-full-width "
            width="80%"
          >
            <img
              src="/assets/home/intro.png"
              className="homeintro"
              style={{ width: "50%" }}
            />
            <Flex direction="column" padding="2em">
              <Text.H4 weight={500} fontColor="primary">
                บริษัท เอส.สตีล เซ็นเตอร์ จำกัด
              </Text.H4>
              <Text.H5 margin="1em 0" lineHeight="30px" fontColor="primary">
                ก่อตั้งเมื่อปี พ.ศ. 2537 <br />
                ดำเนินธุรกิจมาเป็นระยะเวลามากกว่า 20 ปี
                <br />
                <br />
                ตั้งอยู่เลขที่ 28/6 หมู่ที่ 1 ต.หน้าไม้ อ.ลาดหลุมแก้ว จ.ปทุมธานี
                12140 ทางบริษัทฯได้จัดคลังสินค้าขนาดใหญ่ เนื้อที่ใช้สอยกว่า
                9,124 ตรม.
                สามารถรองรับคำสั่งซื้อที่หลากหลายทั้งในแง่ของปริมาณและชนิดของสินค้า
                <br />
                <br />
                จุดประสงค์ในการดำเนินธุรกิจเป็นตัวแทนจำหน่ายเหล็กเส้น
                เหล็กรูปพรรณทุกชนิด อาทิเช่น เหล็กตัวซี เหล็กฉาก เหล็กรางน้ำ
                เหล็กสี่เหลี่ยมตัน เหล็กท่อเหลี่ยม เหล็กท่อแบน เหล็กท่อดำ
                เหล็กเส้นกลม เหล็กข้ออ้อย ลวด ตะปู I-Beam H-Beam Wide Flange
                เหล็กแบน เหล็กแผ่นดำ เหล็กแผ่นลาย เหล็กกัลวาไนซ์ เหล็กไวร์เมช
                เหล็กสำหรับงานโครงสร้างและเหล็กรูปพรรณทุกประเภท
                <br />
                <br />
                โดยเราได้มุ่งเน้นให้ความสำคัญทั้งในด้านคุณภาพสินค้าและบริการ
                การจัดส่งสินค้าที่ตรงต่อเวลาและรวดเร็ว
                เพื่อความพึงพอใจสูงสุดของลูกค้า
              </Text.H5>
              <Link href="/contact">
                <a>
                  <Button
                    margin="0.5em 0 4em 0"
                    width="150px"
                    bgColor="secondary"
                    color="white"
                  >
                    ติดต่อเรา
                  </Button>
                </a>
              </Link>
            </Flex>
          </Flex>
        </Flex>
      </Container>
    </Section>
  );
};
